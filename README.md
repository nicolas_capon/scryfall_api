# scryfall_api

Python wrapper for api.scryfall.com

## How to import as a submodule

```bash
git submodule add https://gitlab.com/nicolas_capon/scryfall_api.git
```

To use in your python application import like this:
```python
from scryfall_api.Scryfall import Scryfall, Expansion, Card
```

